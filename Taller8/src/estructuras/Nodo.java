package estructuras;

/**
 * Interface que representa un nodo de un grafo.
 * @author SamuelSalazar
 *
 */
public interface Nodo extends Comparable<Nodo> {
	
	/**
	 * Identificador único del nodo
	 * @return id
	 */
	public int darId();
	

}
